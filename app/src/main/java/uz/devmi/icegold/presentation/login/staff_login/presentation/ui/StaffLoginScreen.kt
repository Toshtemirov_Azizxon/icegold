package uz.devmi.icegold.presentation.login.staff_login.presentation.ui

import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import uz.devmi.icegold.R

@AndroidEntryPoint
class StaffLoginScreen : Fragment(R.layout.fragment_staff_login){
}