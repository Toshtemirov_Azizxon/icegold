package uz.devmi.icegold.presentation.login.company_login.presentation.ui

import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint
import uz.devmi.icegold.R

@AndroidEntryPoint
class CompanyLoginScreen : Fragment(R.layout.fragment_company_login) {

}