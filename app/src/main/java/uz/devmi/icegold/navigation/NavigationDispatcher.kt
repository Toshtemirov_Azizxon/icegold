package uz.devmi.icegold.navigation

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NavigationDispatcher @Inject constructor() : AppNavigator, NavigationHandler {
    override val navBuffer = MutableSharedFlow<NavArgs>()

    private suspend fun navigate(args:NavArgs){
        navBuffer.emit(args)
    }

    override suspend fun navigateTo(screen: AppScreen) {

    }

    override suspend fun back() {

    }


}