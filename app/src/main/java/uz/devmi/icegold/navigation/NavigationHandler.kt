package uz.devmi.icegold.navigation

import androidx.navigation.NavController
import kotlinx.coroutines.flow.Flow


typealias NavArgs = NavController.()->Unit
interface NavigationHandler {
    val navBuffer:Flow<NavArgs>
}